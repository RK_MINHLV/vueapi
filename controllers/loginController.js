const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const User = mongoose.model('User');
const Bcrypt = require("bcryptjs");

router.post('/', (req, res) => {
    let out = {
        "status" : 1,
        "message" : "",
        "data" : []
    };
    if (req.body.username === undefined || req.body.password === undefined) {
        return res.status(500).end();
    }
    let user = {
        username: req.body.username
    };
    User.findOne(user, (err, doc) => {
        if (!err) {
            if (!doc) {
                res.status(500).end();      
            } else {
                out.data = doc;
                Bcrypt.compare(req.body.password, doc.password).then(function(isValid) {                    
                    if (isValid) {
                        res.send(doc);  
                    } else {
                        res.status(500).end();
                    }
                });
            }
        } else {
            res.status(500).end();     
        }        
    });
});

module.exports = router;

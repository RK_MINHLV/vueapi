const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const Account = mongoose.model('Account');

router.post('/', (req, res) => {
    if (!req.body._id || req.body._id === "") {
        insertRecord(req, res);
    }else {
        updateRecord(req, res);
    }
});


function insertRecord(req, res) {
    let account = new Account();
    account.firstname = req.body.firstname;
    account.lastname = req.body.lastname;
    account.email = req.body.email;
    account.age = req.body.age;
    account.gender = req.body.gender;
    account.address = req.body.age;
    account.employer = req.body.employer;
    account.city = req.body.city;
    account.state = req.body.state;

    account.save((err, doc) => {
        if (!err) {
            let out = {
                "status": 1,
                "message": "Success",
                "data": []
            };
            res.end(JSON.stringify(out));
        }
        else {
            let out = {
                "status": 0,
                "message": "Error",
                "data": []
            };
            res.end(JSON.stringify(out));
        }
    });
}

function updateRecord(req, res) {
    Account.findOneAndUpdate({ _id: req.body._id }, req.body, { new: true }, (err, doc) => {
        let out = {
            "status" : 1,
            "message" : "",
            "data" : []
        };
        if (!err) {
            out.message = "Update success";
        }
        else {
            out.status = 0;
            out.message = "Update fail";
        }
        res.end(JSON.stringify(out));
    });
}


router.get('/list',async (req, res) => {
    let page = req.query.page | 1;
    let skip = (page - 1) * 10;


    console.log(skip);
    Account.find((err, docs) => {
        if (!err) {
            let out = {
                "status": 0,
                "message" : "",
                "data": docs,
                "page": page
            };
            res.end(JSON.stringify(out));
        }
        else {
            console.log('Error in retrieving Account list :' + err);
        }
    }).skip(skip).limit(10);
});

router.get('/delete/:id', (req, res) => {
    Account.findByIdAndRemove(req.params.id, (err, doc) => {
        let out = {
            "status" : 1,
            "message" : "",
            "data" : []
        };
        if (!err) {
            out.message = "Delete success";
        } else {
            out.status = 0;
            out.message = "Delete fail";
        }
        res.end(JSON.stringify(out));
    });
});

router.post('/search', (req, res) => {
    let searchCond = {};
    Object.keys(req.body).forEach(key => {
       if (req.body[key] !== "") {
           searchCond[key] = req.body[key];
       }
    });
    console.log(searchCond);
    Account.find(searchCond, (err, doc) => {
        let out = {
          "status": 1,
          "message" : "",
          "data": []
        };
        if (err) {
            out.status = 0;
            out.message = "Error";
        } else {
            out.data = doc;
        }
        res.end(JSON.stringify(out));
    });
});

module.exports = router;

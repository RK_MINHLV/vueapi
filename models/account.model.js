const mongoose = require('mongoose');

const accountSchema = new mongoose.Schema({
    account_number: {
        type: Number
    },
    balance: {
        type: Number
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    age: {
        type: Number
    },
    gender: {
        type: String
    },
    address: {
        type: String
    },
    employer: {
        type: String
    },
    email: {
        type: String,
        unique: true
    },
    city: {
        type: String
    },
    state: {
        type: String
    }
});

// accountSchema.pre('save', function (next) {
//     let self = this;
//     Account.find({email : self.email}, function (err, docs) {
//         if (!docs.length){
//             next();
//         }else{
//             console.log('account exists: ',self.username);
//             next(new Error("Account exists!"));
//         }
//     });
// }) ;

// Custom validation for email
// accountSchema.path('email').validate((val) => {
//     emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//     return emailRegex.test(val);
// }, 'Invalid e-mail.');

mongoose.model('Account', accountSchema);

